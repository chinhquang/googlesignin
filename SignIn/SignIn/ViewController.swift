//
//  ViewController.swift
//  SignIn
//
//  Created by Night Dog on 11/5/18.
//  Copyright © 2018 NIGHTDOG. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import Google
//class ViewController: UIViewController,GIDSignInDelegate, GIDSignInUIDelegate {
//
//    @IBAction func GoogleSignIn(_ sender: Any) {
//        GIDSignIn.sharedInstance()?.signIn()
//    }
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        print("Google Sing In didSignInForUser")
//        if let error = error {
//            print(error.localizedDescription)
//            return
//        }
//        guard let authentication = user.authentication else { return }
//        let credential = FIRGoogleAuthProvider.credential(withIDToken: (authentication.idToken)!, accessToken: (authentication.accessToken)!)
//        // When user is signed in
//        FIRAuth.auth()?.signIn(with: credential, completion: { (user, error) in
//            if let error = error {
//                print("Login error: \(error.localizedDescription)")
//                return
//            }
//        })
//
//    }
//    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
//        dismiss(animated: true) {() -> Void in }
//
//    }
//    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        if let aController = viewController {
//            present(aController, animated: true) {() -> Void in }
//        }
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        var error : NSError?
//        GGLContext.sharedInstance()?.configureWithError(&error)
//        if error != nil {
//            print(error as Any)
//
//        }
//
//        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance()?.clientID = "24937570262-snae35sutn7vtab1sbmjemfqd1geft0d.apps.googleusercontent.com"
//    }
//
//
//}

class ViewController: UIViewController, UISearchBarDelegate, GIDSignInUIDelegate,GIDSignInDelegate{
    //SIGNIN
    static var fullname = String()
    static var isSignOut : Int = 0
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        
        print("Google Sign In didSignInForUser")
        if let error = error {
            print(error.localizedDescription)
            return
        }else{
            ViewController.fullname = user.profile.name
            
        }
        guard let authentication = user.authentication else { return }
        let credential = FIRGoogleAuthProvider.credential(withIDToken: (authentication.idToken)!, accessToken: (authentication.accessToken)!)
        // When user is signed in
        FIRAuth.auth()?.signIn(with: credential, completion: { (user, error) in
            if let error = error {
                print("Login error: \(error.localizedDescription)")
                return
            }
        })
        performSegue(withIdentifier: "UserSegue", sender: self)
        
    }
    
    //PRESENT
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }
    }
    //DISMISS
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true) {() -> Void in }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        if(ViewController.isSignOut == 1){
            GIDSignIn.sharedInstance()?.signOut()
            ViewController.isSignOut = 0;
        }
        
        if FIRAuth.auth()?.currentUser != nil {
            
            
            performSegue(withIdentifier: "UserSegue", sender: self)
        } else {
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance()?.clientID = "24937570262-snae35sutn7vtab1sbmjemfqd1geft0d.apps.googleusercontent.com"
            
            var error : NSError?
            GGLContext.sharedInstance()?.configureWithError(&error)
            if error != nil {
                print(error as Any)
                
            }
            let signInButton = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50));
            signInButton.center = view.center
            view.addSubview(signInButton)
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        
    }
    
}

