//
//  UserViewController.swift
//  SignIn
//
//  Created by Night Dog on 11/7/18.
//  Copyright © 2018 NIGHTDOG. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
class UserViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ViewController.fullname
    self.navigationItem.setHidesBackButton(true, animated:true);
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signOutSegue"{
            ViewController.isSignOut = 1
            
        }
    }
  
}
